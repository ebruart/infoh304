#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <random>
#include <unordered_set>
extern "C" {
#include <UbigraphAPI.h>
}

using namespace std;

class BinarySearchTree
{
private:
	struct node
	{
		node *left;
		node *right;
		node *parent;
		int content;
	};
	node* root;
	node* tree_search(int content);
	node* tree_search(int content, node* location);
	void delete_no_child(node* location);
	void delete_left_child(node* location);
	void delete_right_child(node* location);
	void delete_two_children(node* location);
	
public:
	BinarySearchTree ()
	{
		root=NULL;
	}
	bool isEmpty()
	{
		return root==NULL;
	}
	void insert_element(int content);
	void delete_element(int content);
	void inorder(node* location);
	void print_inorder();
};


void BinarySearchTree::insert_element(int content)
{
	//New node
	node* n = new node();
	n->content = content;
	n->left = NULL;
	n->right = NULL;
	n->parent = NULL;
	
	//For visualization
	int eid,vid;
	this_thread::sleep_for(chrono::milliseconds(100));
	ubigraph_new_vertex_w_id(content);
	ubigraph_set_vertex_attribute(content, "color", "#0000ff");
	ubigraph_set_vertex_attribute(content, "label", to_string(content).c_str());
	
	if(isEmpty())
	{
		root = n;
		ubigraph_set_vertex_attribute(content, "color", "#ff0000");
		this_thread::sleep_for(chrono::milliseconds(100));
		ubigraph_set_vertex_attribute(content, "color", "#0000ff");
		this_thread::sleep_for(chrono::milliseconds(100));
	}
	else
	{
		
		node* pointer = root;
		
		while ( pointer != NULL )
		{
			n->parent = pointer;
			
			if(n->content > pointer->content)
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->right;
				
			}
			else
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->left;
				
			}
		}
		
		if ( n->content < n->parent->content )
		{
			n->parent->left = n;
			this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");
			
		}
		else
		{
			n->parent->right = n;
			this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");		
		}
	}
}

void BinarySearchTree::inorder(node* location)
{
	if (location != NULL)
	{
		inorder(location->left);
		cout << location->content << " , ";
		inorder(location->right);
	}
}

void BinarySearchTree::print_inorder()
{
	cout << "Contenu de l'arbre : ";
	inorder(root);
	cout << endl;
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content)
{
	return tree_search(content, root);
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content, node* location)
{
	node* temp = location; 
	while (temp!=NULL && (temp->content)!=content)
	{
		if (temp->content < content)
			temp = temp->right; 
		else if (temp->content > content)
			temp = temp->left; 
	}
	if(temp == NULL)
	{
		cout << content << " n'est pas dans l'arbre" << endl; 
		return NULL; 
	}
	else
	{
		cout << "j'ai trouvé " << content << endl; 
		return temp; 
	}
}

void BinarySearchTree::delete_no_child(node* location)
{
	if (location->parent != NULL)
	{
		if (location->content > location->parent->content)
			location->parent->right = NULL;
			delete location;  
		else if (location->content > location->parent->content)
			location->parent->left = NULL; 
			delete location; 
	}
	else 
		root = NULL;
		delete location;  
}

void BinarySearchTree::delete_left_child(node* location)
{
	location->content = location->left->content; 
	delete_element(location->left->content);
	
}

void BinarySearchTree::delete_right_child(node* location)
{
	location->content = location->right->content; 
	delete_element(location->right->content);
}

void BinarySearchTree::delete_two_children(node* location)
{
	
}

void BinarySearchTree::delete_element(int content)
{
	node* todelete = tree_search(content); 
	if (todelete == NULL)
		cout << "Cet élément n'est pas dans l'arbre" << endl; 
	else 
	{ 
		if (todelete->left==NULL && todelete->right==NULL) //cas pas d'enfants 
			delete_no_child(todelete); 
		else if (todelete->left!=NULL && todelete->right==NULL)
			delete_left_child(todelete); 
		else if (todelete->left==NULL && todelete->right!=NULL)
			delete_right_child(todelete);
		else 
			delete_two_children(todelete);		
	}
}


int main()
{
	ubigraph_clear();
	unordered_set<int> vertices;
	int vertex;
	BinarySearchTree bst;
	
	
	//random_device rd;
	//mt19937 gen(rd());
	//uniform_int_distribution<int> r(0, 1000);
	
	//for ( int i=1; i<=10; i++ )
	//{
		//vertex = r(gen);
		//if ( vertices.count(vertex) == 0 )
		//{
			//vertices.insert(vertex);
			//bst.insert_element(vertex);
		//}
		//else
			//i--;
	//}
	
	bst.insert_element(50);
	bst.insert_element(30);
	bst.insert_element(60);
	bst.insert_element(25);
	bst.insert_element(27);
	bst.insert_element(40);
	bst.insert_element(35);
	bst.insert_element(70);
	bst.insert_element(55);
	bst.insert_element(59);
	
	bst.delete_element(59);  
	
	return 0;
}
