#include <iostream>
#include <vector>
#include <cstdlib>

#define INFINITY 1000

using namespace std; 

int NTot (int m) { //algorithme glouton
	
	vector<int> valeurs = {1,2,5,10,20};
	int n = valeurs.size(); 
	
	if (m<0)
	{
		return INFINITY;
	}
	else if (m == 0)
			return 0; 
	else 
	{
		int i=0; 
		while (i<n-1 && valeurs[i+1]<=m)
		{
			i+= 1; 
		}
		return NTot(m-valeurs[i])+1;
		}
	}
	
int bestNTot (int m) {
	
	vector<int> valeurs = {1,2,5,10,20};
	int n = valeurs.size(); 
	
	if (m<0)
	{
		return INFINITY;
	}
	else if (m == 0)
			return 0; 
	else 
	{
		int min = INFINITY; 
		for (int i=0; i<n; i++)
		{
			int current = bestNTot(m-valeurs[i])+1;
			if (current < min)
				min = current; 
		}
		return min; 
	}
}

int main(int argc, const char *argv[])
{
	
	if(argc == 1)
		cout <<"valeur svp"<<endl; 
		
	else
	{
		int m = atoi(argv[1]);
		int best = bestNTot(m); 
		
		if (best >= INFINITY)
			cout << " Impossible de rendre le montant m" << endl; 
			
		else 
			cout << best << " pièces sont nécessaires" << endl; 
		}

	return 0; 
}

	
