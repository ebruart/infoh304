#ifdef OSX
#include <GLUT/glut.h>
#elif LINUX
#include <GL/glut.h>
#endif
#include <iostream>
#include <string>
#include <set>
#include <utility>
#include <random>
#include <fstream>

using namespace std;

set<string> words;
GLint* points;
int size = 0;

void init(float r, float g, float b)
{
	glClearColor(r,g,b,0.0);  
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D (1.0, 128.0, 1.0, 128.0);
}

unsigned long long pre_hash(string word){
	unsigned long long preh = 0; 
	for (int i=0; i<word.size(); i++)
	{
		unsigned long long temp = word[i];
		temp = temp*(unsigned long long)pow(2,i*7); //on recule le nombre du nombre de cases déjà occupées
		preh += temp; 
	}
	return preh; 
	
}

pair<int,int> hash_function(string word)
{
	//définition 
	unsigned long long preh, h, a; 
	int x, y, m, w, r;
	pair<int,int> coord;
	
	preh = pre_hash(word);
	
	//random_device rd;
	//mt19937 gen(rd());
	//uniform_int_distribution<int> r(1, 128);
	
	//technique du modulo
	//m = 567890345;
	//h = preh%m; 
	
	//technique de la multiplication 
	a = 567890345;
	w = 64;
	r = 14;
	h = (a*preh)>>(w-r); //en le décalrant sur 64 bits le shift est automatique 
	
	x = (h%128)+1;           //on prend les 7 premiers bits 
	y = (h>>7)%128 +1;
	
	coord = make_pair(x,y);
	return coord;
}

void make_points()
{
	pair<int,int> coord;
	int i=0;
	for (set<string>::iterator it=words.begin(); it!=words.end(); ++it)
	{
		coord = hash_function(*it);
		points[i++] = (GLint)coord.first;
		points[i++] = (GLint)coord.second;	
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	glViewport(128.0,128.0,512.0,512.0);
	glPointSize(2.0f);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_INT, 0, points);
	glDrawArrays(GL_POINTS,0,size);
	glDisableClientState(GL_VERTEX_ARRAY);
	glutSwapBuffers();
}

int main(int argc,char *argv[])
{
	
	string word;

	if ( argc != 2 )
	{
		cout << "Enter a file name." << endl;
	}
	else
	{
		ifstream file (argv[1]);
		
		string str;
		while ( file >> word )
		{
			words.insert(word);
		}
		size = words.size();
		cout << "Document size: " << words.size() << " words" << endl;

		points = (GLint *)malloc(2*size*sizeof(GLint));
		make_points();
		glutInit(&argc,argv);
		glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
		glutInitWindowSize (768, 768);
		glutInitWindowPosition (200, 200);
		glutCreateWindow ("Hash Function Visualization");
		init(0.0,0.0,0.0);
		glutDisplayFunc(display);
		glutMainLoop();
	}
	return 0;
}
